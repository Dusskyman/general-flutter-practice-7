import 'package:flutter/material.dart';
import 'package:general_flutter_practice_7/custom_paint_drawline.dart';

class LineSlider extends StatefulWidget {
  final double height;
  final double width;

  const LineSlider({this.height = 50.0, this.width = 350});

  @override
  _LineSliderState createState() => _LineSliderState();
}

class _LineSliderState extends State<LineSlider> {
  double drapPos = 0;
  double procent = 0;

  void _updateDragPosition(Offset val) {
    double newDragPos = 0;
    if (val.dx <= 0) {
      newDragPos = 0;
    } else if (val.dx >= widget.width) {
      newDragPos = widget.width;
    } else {
      newDragPos = val.dx;
    }
    setState(() {
      drapPos = newDragPos;
      procent = (drapPos / widget.width) * 100;
    });
  }

  void _onDragUpdate(BuildContext context, DragUpdateDetails update) {
    RenderBox box = context.findRenderObject();
    Offset offset = box.globalToLocal(update.globalPosition);
    _updateDragPosition(offset);
    // print(offset.dx);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('${procent.floorToDouble()}'),
        GestureDetector(
          child: Container(
              // color: Colors.green,
              width: widget.width,
              height: 50,
              child: CustomPaint(
                painter: CustomPainterDrawLine(
                  color: Colors.red,
                  sliderValue: drapPos,
                  procent: procent,
                ),
              )),
          onHorizontalDragUpdate: (DragUpdateDetails update) =>
              _onDragUpdate(context, update),
        ),
      ],
    );
  }
}
