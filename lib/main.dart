import 'package:flutter/material.dart';
import 'package:general_flutter_practice_7/custom_paint_drawline.dart';
import 'package:general_flutter_practice_7/lineSlider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: LineSlider(
        width: MediaQuery.of(context).size.width * 0.75,
      )),
    );
  }
}
